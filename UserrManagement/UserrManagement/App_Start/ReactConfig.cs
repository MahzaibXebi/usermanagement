using React;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(UserrManagement.ReactConfig), "Configure")]

namespace UserrManagement
{
	public static class ReactConfig
	{
        public static void Configure()
        {

            var compilationSection = (System.Web.Configuration.CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");

            if (compilationSection.Debug)
            {
                // If you want to use server-side rendering of React components, 
                // add all the necessary JavaScript files here. This includes 
                // your components as well as all of their dependencies.
                // See http://reactjs.net/ for more information. Example:
                ReactSiteConfiguration.Configuration
                    .SetLoadReact(true)
                    .SetReuseJavaScriptEngines(false)
                    .AddScript("~/Script/Application/User.jsx")
                    //.AddScript("~/script/react/react.min.js")
                    //.AddScript("~/script/react/react-dom.min.js")
                    //.AddScript("~/script/react/react-router.js")
                    //.AddScript("~/Scripts/Second.jsx")
                    ;

                // If you use an external build too (for example, Babel, Webpack,
                // Browserify or Gulp), you can improve performance by disabling 
                // ReactJS.NET's version of Babel and loading the pre-transpiled 
                // scripts. Example:
                //ReactSiteConfiguration.Configuration
                //	.SetLoadBabel(false)
                //	.AddScriptWithoutTransform("~/Scripts/bundle.server.js")
            }
            else
            {
                ReactSiteConfiguration.Configuration
                    //.SetLoadReact(false)
                    .SetReuseJavaScriptEngines(true)
                    .AddScript("~/Script/Application/User.jsx")
                    //.AddScript("~/script/react/react.min.js")
                    //.AddScript("~/script/react/react-dom.min.js")
                    //.AddScript("~/script/react/react-router.js")
                    //.AddScript("~/Scripts/Second.jsx")
                    ;
            }
        }
    }
}