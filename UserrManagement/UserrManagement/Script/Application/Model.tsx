﻿interface IUserState {
}

interface IUser {
    FirstName: string;
    LastName: string;
    Address: string;
    Role: string;
    Email: string;
    Password: string;
    ConfirmPassword: string;   
}
  